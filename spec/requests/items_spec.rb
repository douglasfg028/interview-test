require "rails_helper"

RSpec.describe ItemsController, type: :request do
  def body
    JSON.parse(response.body)
  end

  it "ticks an empty list" do
    post "/items/tick",
      params: { items: [] }.to_json,
      headers: { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
    expect(body).to eq []
  end

  it "ticks an list with Gorgonzola cheese with quality 10 and days remaining 5" do
    post "/items/tick",
         params: { items: [{ id: 1, type: 'Gorgonzola cheese', daysRemaining: 5, quality: 10 }] }.to_json,
         headers: { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }
    expect(body).to eq [{ "id" => 1, "type" => 'Gorgonzola cheese', "daysRemaining" => 4, "quality" => 11 }]
  end
end
