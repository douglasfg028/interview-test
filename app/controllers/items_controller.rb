class ItemsController < ApplicationController
  skip_before_action :verify_authenticity_token

  def tick
    new_items = params[:items].collect do |item|
      Item.tick(
        id: item[:id].to_i,
        type: item[:type],
        days_remaining: item[:daysRemaining].to_i,
        quality: item[:quality].to_i
      )
    end

    render json: new_items
  end
end