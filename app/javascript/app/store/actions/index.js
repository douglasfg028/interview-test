import { ADD_ITEM, TICK } from './../constants/actionTypes';

let nextId = 2;

export const addItem = (type, quality) => ({
  type: ADD_ITEM,
  item: {
    id: nextId++,
    type,
    quality,
    daysRemaining: 20,
  }
});

export const tick = () => {
  return (dispatch, getStore) => {
    return fetch('/items/tick', {
      method: "POST",
      body: JSON.stringify(getStore()),
      headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    }
  }).then(response => {
    response.json().then(data => {
      dispatch({items: data, type: 'UPDATE_ITEMS'});
      dispatch({type: TICK});
    })
  });
  }
}

function handleErrors(response) {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
}
