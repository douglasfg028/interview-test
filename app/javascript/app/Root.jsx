import React from 'react';
import Navbar from './components/Navbar';
import Items from './components/Items';
import AddItem from './components/AddItem';
import Ticker from './components/Ticker';

import { addItem, tick } from './store/actions';

const Root = ({ store }) => {
  const state = store.getState();
  return (
    <React.Fragment>
      <Navbar daysElapsed={state.daysElapsed} />

      <div className="root">
        <AddItem
          onAdd={(type, quality) =>
            store.dispatch(addItem(type, quality))
          }
        />
        <Items items={state.items} />
      </div>

      <Ticker onTick={() => store.dispatch(tick())} />
    </React.Fragment>
  );
};

export default Root;
