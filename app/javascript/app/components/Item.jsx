import React from 'react';

const Item = ({ item }) => (
  <div className="item">
    <div className="item__info">
      <span className="item__id">#{item.id}</span>
      <span className="item__type">{item.type}</span>
    </div>
    <div className="item__features">
      <span>Quality: {item.quality}</span>
      <span>Days remaining: {item.daysRemaining}</span>
    </div>
  </div>
);

export default Item;
