class Item
  def initialize(id:, type:, days_remaining:, quality:)
    @id = id
    @type = type
    @days_remaining = days_remaining
    @quality = quality
  end

  def self.tick(id:, type:, days_remaining:, quality:)
    new(id: id, type: type, days_remaining: days_remaining, quality: quality).tick
  end

  def tick
    process_item
    struct_item
  end

  private

  attr_reader :type, :days_remaining, :quality

  def struct_item
    {
      id: @id,
      type: @type,
      daysRemaining: @days_remaining,
      quality: @quality
    }
  end

  def process_item
    case @type
    when 'Gorgonzola cheese'
      process_gorgonzola_cheese
    when 'Concert ticket'
      process_concert_ticket
    when 'Gold ring'
      process_gold_ring
    when 'Slice of bread'
      process_slice_of_bread
    else
      process_others
    end
  end

  def process_gorgonzola_cheese
    if @quality >= 49
      @quality = 50
    elsif @days_remaining > 0 && @quality < 50
      @quality += 1
    elsif @days_remaining <= 0
      @quality += 2
    end

    @days_remaining -= 1
  end

  def process_concert_ticket
    if @days_remaining <= 0
      @quality = 0
    elsif @quality >= 49
      @quality = 50
    elsif @days_remaining > 10
      @quality += 1
    elsif @days_remaining > 5
      @quality += 2
    else
      @quality += 3
    end

    @days_remaining -= 1
  end

  def process_gold_ring
    @quality = @quality
    @days_remaining = @days_remaining
  end

  def process_slice_of_bread
    if @quality == 0
      @quality = 0
    elsif @days_remaining > 0
      @quality -= 2
    else
      @quality -= 4
    end

    @days_remaining -= 1
  end

  def process_others
    if @quality == 0
      @quality = 0
    elsif @days_remaining <= 0
      @quality -= 2
    elsif @days_remaining > 0
      @quality -= 1
    end

    @days_remaining -= 1
  end
end